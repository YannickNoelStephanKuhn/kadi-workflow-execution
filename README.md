# kadi-workflow-execution

## Description
Helper scripts that demonstrate and run the Kadi4Mat process_manager [https://gitlab.com/iam-cms/workflows/process-manager](https://gitlab.com/iam-cms/workflows/process-manager) in the simplest implementation possible.

## Installation
To use these scripts as a method of your web browser opening workflow files, you have to compile them with pyinstaller; see [https://pyinstaller.org/](https://pyinstaller.org/). Then register the produced executable as a file opening method, without removing it from the other files in its folder.

## Roadmap
Upload workflow file suitable for unit testing.

## Authors and acknowledgment
Yannick Kuhn, German Aerospace Center

## License
Apache License 2.0
