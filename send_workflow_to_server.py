"""
Copyright 2023 German Aerospace Center

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import sys
import os

# Put the URL and port of the REST API server here.
url = "localhost"
port = 8080

file_name = sys.argv[1]

output = os.popen(
    'python execute_workflow_remotely.py "'
    + file_name
    + '" -a '
    + url
    + ' - p '
    + str(port)
).read()
print(output)
input("Press any key to close the window.")
